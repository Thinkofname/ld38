
use std::f64::consts::PI;

pub fn normalize_angle(a: f64) -> f64 {
    (PI * 2.0 + (a  % (PI * 2.0))) % (PI * 2.0)
}

pub fn angle_distance(a: f64, b: f64) -> f64 {
    let a = normalize_angle(a);
    let b = normalize_angle(b);
    let diff = (a - b).abs();
    if diff > PI {
        (PI * 2.0) - diff
    } else {
        diff
    }
}