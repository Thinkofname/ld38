
use js::{self, Context2d, Canvas};
use std::f64::consts::PI;
use util;
use planet::Planet;

extern "C" {
    fn play_shoot();
}

pub struct Object {
    pub otype: ObjectType,
    pub distance: f64,
    pub angle: f64,

    pub width: f64,
    pub height: f64,

    pub velocity: f64,
    scale: f64,
}

impl Object {
    pub fn new(otype: ObjectType, distance: f64, angle: f64) -> Object {
        let (width, height) = otype.size();

        Object {
            otype: otype,
            distance: distance,
            angle: angle,
            velocity: 0.0,
            scale: 1.0,

            width: width,
            height: height,
        }
    }

    pub fn tick(&mut self, p_pos: (f64, f64), p_rad: f64, projectiles: &mut Vec<Projectile>, delta: f64) {
        let angle = self.angle - (PI/2.0);
        let rad = self.width.max(self.height);
        let x = self.distance * angle.cos();
        let y = self.distance * angle.sin();
        if (x*x + y*y) - (p_rad+rad)*(p_rad+rad) > 0.5 {
            self.velocity -= 0.05 * delta;
        } else {
            if self.velocity < 0.0 {
                self.velocity = 0.0;
            }
            self.distance = p_rad + rad;
        }
        if self.velocity < -1.5 {
            self.velocity = -1.5;
        }

        self.distance += self.velocity * delta;

        let target_scale = if self.velocity.abs() < 0.01 {
            1.0
        } else {
            1.0 / self.velocity.abs()
        };
        self.scale += (target_scale - self.scale) * delta * 0.1;


        let self_x = p_pos.0 + self.distance * (self.angle - (PI/2.0)).cos();
        let self_y = p_pos.1 + self.distance * (self.angle - (PI/2.0)).sin();

        match self.otype {
            ObjectType::BasicGun{
                ref mut cooldown,
                ref mut max_cooldown,
                ref mut initial_fire_delay,
                ref mut shooting
            } => {
                if *cooldown > 0.0 {
                    *cooldown -= delta;
                } else {
                    *cooldown = 0.0;
                }
                let mouse = js::get_mouse_state();
                let mut firing = mouse.buttons & 0b1 == 1;

                let canvas = Canvas::get_by_id("main").unwrap();
                let angle = ((mouse.client_x - (canvas.get_width() / 2)) as f64)
                    .atan2((mouse.client_y - (canvas.get_height() / 2)) as f64)
                    - PI;
                let angle = -angle;

                if util::angle_distance(self.angle, angle) > PI * 0.4 {
                    firing = false;
                }

                if !firing {
                    *shooting = false;
                    return;
                }

                if firing && !*shooting {
                    *shooting = true;
                    if *cooldown <= 0.0 {
                        *cooldown = *initial_fire_delay;
                    }
                }

                if *cooldown < 0.0 {
                    *cooldown = *max_cooldown;
                    unsafe { play_shoot(); }
                    projectiles.push(Projectile {
                        x: self_x,
                        y: self_y,
                        vx: (angle - (PI/2.0)).cos() * 12.0,
                        vy: (angle - (PI/2.0)).sin() * 12.0,
                        angle: (angle - (PI/2.0)),
                        lifetime: 500.0,
                        friendly: true,
                    });
                }
            }
            ObjectType::AIGun{
                ref mut cooldown,
                ref mut max_cooldown,
                ref mut initial_fire_delay,
                ref mut shooting,
                target
            } => {
                if *cooldown > 0.0 {
                    *cooldown -= delta;
                } else {
                    *cooldown = 0.0;
                }
                let mut firing = target.is_some();

                let angle = if let Some((tx, ty)) = target {
                    let angle = ((tx - self_x) as f64)
                        .atan2((ty - self_y) as f64);
                    let angle = PI - angle;

                    if util::angle_distance(self.angle, angle) > PI * 0.4 {
                        firing = false;
                    }
                    angle
                } else { 0.0 };

                if !firing {
                    *shooting = false;
                    return;
                }

                if firing && !*shooting {
                    *shooting = true;
                    if *cooldown <= 0.0 {
                        *cooldown = *initial_fire_delay;
                    }
                }

                if *cooldown < 0.0 {
                    *cooldown = *max_cooldown;
                    unsafe { play_shoot(); }
                    projectiles.push(Projectile {
                        x: self_x,
                        y: self_y,
                        vx: (angle - (PI/2.0)).cos() * 12.0,
                        vy: (angle - (PI/2.0)).sin() * 12.0,
                        angle: (angle),
                        lifetime: 500.0,
                        friendly: false,
                    });
                }
            }
            _ => {},
        }
    }

    pub fn draw(&mut self, ctx: &Context2d) {
        ctx.save();
        let angle = self.angle - (PI/2.0);
        ctx.translate(
            (self.distance * angle.cos()) as i32,
            (self.distance * angle.sin()) as i32,
        );
        ctx.rotate(self.angle);
        let scale = self.scale.min(1.0).max(0.1);
        ctx.scale(scale, 1.0 / scale);

        self.otype.draw(ctx);

        ctx.restore();
    }
}

pub enum ObjectType {
    Controller,
    BasicGun {
        initial_fire_delay: f64,
        max_cooldown: f64,
        cooldown: f64,
        shooting: bool,
    },
    AIGun {
        initial_fire_delay: f64,
        max_cooldown: f64,
        cooldown: f64,
        shooting: bool,
        target: Option<(f64, f64)>,
    },
}

impl ObjectType {
    fn size(&self) -> (f64, f64) {
        match *self {
            ObjectType::BasicGun{..} => (8.0, 8.0),
            ObjectType::AIGun{..} => (8.0, 8.0),
            ObjectType::Controller => (8.0, 8.0),
        }
    }

    fn draw(&mut self, ctx: &Context2d) {
        match *self {
            ObjectType::Controller => {
                ctx.set_fill_style("#FFFF00");
                ctx.fill_rect(-4, -4, 8, 8);
            }
            ObjectType::BasicGun{cooldown, max_cooldown, ..} => {
                ctx.set_fill_style(&format!("rgb(255, {green}, 0)",
                    green = ((cooldown / max_cooldown) * 255.0) as i32
                ));
                ctx.fill_rect(-4, -4, 8, 8);
            }
            ObjectType::AIGun{cooldown, max_cooldown, ..} => {
                ctx.set_fill_style(&format!("rgb(255, {green}, 0)",
                    green = ((cooldown / max_cooldown) * 255.0) as i32
                ));
                ctx.fill_rect(-4, -4, 8, 8);
            }
        }
    }
}

pub struct Projectile {
    pub x: f64,
    pub y: f64,
    pub vx: f64,
    pub vy: f64,
    pub angle: f64,
    pub lifetime: f64,
    pub friendly: bool,
}

impl Projectile {

    pub fn tick(&mut self, delta: f64, planets: &mut [Planet]) {
        self.x += self.vx * delta;
        self.y += self.vy * delta;
        self.lifetime -= delta;

        if self.friendly {
            for p in &mut planets[1..] {
                let dx = self.x - p.x;
                let dy = self.y - p.y;
                let r = p.radius + 1.0;
                if dx*dx + dy*dy <= r*r {
                    self.lifetime = 0.0;
                    p.health -= 10.0;
                    p.damage_cooldown = 400.0;
                }
            }
        } else {
            let p = &mut planets[0];
            let dx = self.x - p.x;
            let dy = self.y - p.y;
            let r = p.radius + 1.0;
            if dx*dx + dy*dy <= r*r {
                self.lifetime = 0.0;
                p.health -= 8.0;
                p.damage_cooldown = 200.0;
            }
        }
    }

    pub fn should_remove(&self) -> bool {
        self.lifetime <= 0.0
    }

    pub fn draw(&mut self, ctx: &Context2d) {
        ctx.save();
        let angle = self.angle - (PI/2.0);
        ctx.translate(self.x as i32, self.y as i32);
        ctx.rotate(angle);

        ctx.set_fill_style(if self.friendly { "#00FF00" } else { "#FF00FF" });
        ctx.fill_rect(-2, -2, 4, 4);

        ctx.restore();
    }
}