
use js::{Canvas, Context2d};
use rand::{thread_rng, Rng};
use std::f64::consts::PI;
use object::{Projectile, Object, ObjectType};
use util;

extern "C" {
    fn play_explode();
}

pub struct Planet {
    pub ptype: PlanetType,
    image: Canvas,
    pub radius: f64,
    pub x: f64,
    pub y: f64,
    pub health: f64,
    pub max_health: f64,
    pub damage_cooldown: f64,

    pub vx: f64,
    pub vy: f64,
    pub avx: f64,
    pub avy: f64,

    pub objects: Vec<Object>,
    pub player_owned: bool,
    has_died: bool,
}

impl Planet {
    pub fn new(x: f64, y: f64, radius: f64) -> Planet {
        let ty = PlanetType::random();
        Planet {
            image: ty.generate_image(radius),
            ptype: ty,
            x: x,
            y: y,
            radius: radius,
            vx: 0.0,
            vy: 0.0,
            avx: 0.0,
            avy: 0.0,

            health: 100.0,
            max_health: 100.0,
            damage_cooldown: 0.0,

            objects: vec![],
            player_owned: false,
            has_died: false,
        }
    }

    pub fn near_object(&self, angle: f64) -> Option<usize> {
        self.objects.iter()
            .enumerate()
            .min_by(|&(_, a), &(_, b)| {
                util::angle_distance(angle, a.angle).partial_cmp(&util::angle_distance(angle, b.angle))
                    .unwrap()
            })
            .into_iter()
            .find(|&(_, v)| util::angle_distance(angle, v.angle) * self.radius < v.width.max(v.height))
            .map(|(id, _)| id)
    }

    pub fn ai_tick(&mut self, player: &Planet, delta: f64) {
        let dx = player.x - self.x;
        let dy = player.y - self.y;
        let min_dist = self.radius + player.radius + 320.0;
        let dist = dx.hypot(dy);
        let angle = dy.atan2(dx);
        if dist > min_dist {
            self.vx = (dx / dist) * 7.0;
            self.vy = (dy / dist) * 7.0;
        } else {
            self.vx = 6.7 * (angle + PI * 0.5).cos();
            self.vy = 6.7 * (angle + PI * 0.5).sin();
        }
        for obj in &mut self.objects {
            if let ObjectType::AIGun{ref mut target, ..} = obj.otype {
                *target = if dist > 800.0 { None } else {
                    Some((player.x, player.y))
                };
            }
        }
    }

    pub fn tick(&mut self, projectiles: &mut Vec<Projectile>, delta: f64) {
        if self.health <= 0.0 {
            if !self.has_died {
                unsafe { play_explode(); }
                self.has_died = true;
            }
            return;
        }
        if self.vx.abs() > 20.0 {
            self.vx = self.vx.signum() * 20.0;
        }
        if self.vy.abs() > 20.0 {
            self.vy = self.vy.signum() * 20.0;
        }
        if self.avx.abs() > 20.0 {
            self.avx = self.avx.signum() * 20.0;
        }
        if self.avy.abs() > 20.0 {
            self.avy = self.avy.signum() * 20.0;
        }
        self.avx += (self.vx - self.avx).signum() * delta * 0.08;
        self.avy += (self.vy - self.avy).signum() * delta * 0.08;
        if (self.avx - self.vx).abs() < 0.01 {
            self.avx = self.vx;
        }
        if (self.avy - self.vy).abs() < 0.01 {
            self.avy = self.vy;
        }
        self.x += self.avx * delta;
        self.y += self.avy * delta;
        for o in &mut self.objects {
            o.tick((self.x, self.y), self.radius, projectiles, delta);
        }

        if self.damage_cooldown <= 0.0 {
            self.damage_cooldown = 0.0;
            if self.health < self.max_health {
                self.health = self.max_health.min(self.health + 0.1 * delta);
            } else {
                self.health = self.max_health;
            }
        } else {
            self.damage_cooldown -= delta;
        }
    }

    pub fn draw(&mut self, ctx: &Context2d) {
        if self.player_owned && self.health <= 0.0 {
            return;
        }
        ctx.save();
        ctx.translate(
            self.x as i32,
            self.y as i32
        );
        ctx.draw_image(
            &self.image,
            -(self.image.get_width() / 2),
            -(self.image.get_height() / 2),
        );
        for o in &mut self.objects {
            o.draw(ctx);
        }

        ctx.set_fill_style("#00FF00");
        let w = (((self.radius + 16.0) * 2.0) / self.max_health) * self.health;
        ctx.fill_rect(-self.radius as i32 - 16, -self.radius as i32 - 40, w as i32, 10);

        ctx.restore();
    }

    pub fn increase_size(&mut self, size: f64) {
        if size <= 0.0 {
            return;
        }
        let change = (size / self.radius) * 5.0;
        self.radius += change;
        self.max_health += change * 4.0;
        println!("New size: {}", self.radius);
        self.image = self.ptype.generate_image(self.radius);
        for o in &mut self.objects {
            o.distance += change + 5.0;
        }
    }
}

pub struct PlanetType {
    pub water_color: String,
    water_color_dark: String,
    atmosphere_color: String,
    atmosphere_color_alpha: String,
}

impl PlanetType {

    pub fn random() -> PlanetType {
        let mut rng = thread_rng();

        let h = rng.gen_range(0, 360);
        let s = rng.gen_range(45, 70);
        let l = rng.gen_range(30, 70);

        let water_color = format!("hsl({h}, {s}%, {l}%)",
            h = h, s = s, l = l,
        );
        let water_color_dark = format!("hsl({h}, {s}%, {l}%)",
            h = h, s = s, l = l - 25,
        );
        let h = rng.gen_range(0, 360);
        let s = rng.gen_range(60, 90);
        let l = rng.gen_range(70, 100);
        let atmosphere_color = format!("hsla({h}, {s}%, {l}%, 0.6)",
            h = h, s = s, l = l,
        );
        let atmosphere_color_alpha = format!("hsla({h}, {s}%, {l}%, 0.0)",
            h = h, s = s, l = l,
        );

        PlanetType {
            water_color: water_color,
            water_color_dark: water_color_dark,
            atmosphere_color: atmosphere_color,
            atmosphere_color_alpha: atmosphere_color_alpha,
        }
    }

    pub fn generate_image(&self, radius: f64) -> Canvas {
        let canvas = Canvas::new();

        canvas.set_width((radius * 2.0 + 32.0) as i32);
        canvas.set_height((radius * 2.0 + 32.0) as i32);

        let ctx = canvas.get_context_2d();
        ctx.set_image_smoothing(false);

        ctx.save();
        {
            ctx.begin_path();

            let water = ctx.create_radial_gradient(
                canvas.get_width() / 2, canvas.get_height() / 2, 0,
                canvas.get_width() / 2, canvas.get_height() / 2, radius.ceil() as i32
            );
            water.add_color_stop(0.0, &self.water_color);
            water.add_color_stop(1.0, &self.water_color_dark);

            ctx.set_fill_style(&water);
            ctx.arc(canvas.get_width() / 2, canvas.get_height() / 2, radius.ceil() as i32, 0.0, 2.0 * PI);
            ctx.fill();
            ctx.clip();
        }

        ctx.restore();

        {
            ctx.begin_path();
            let atmosphere = ctx.create_radial_gradient(
                canvas.get_width() / 2, canvas.get_height() / 2, 0,
                canvas.get_width() / 2, canvas.get_height() / 2, radius.ceil() as i32 + 8,
            );
            atmosphere.add_color_stop(0.75f64.min(radius / 50.0), &self.atmosphere_color_alpha);
            atmosphere.add_color_stop(0.9, &self.atmosphere_color);
            atmosphere.add_color_stop(1.0, &self.atmosphere_color_alpha);

            ctx.set_fill_style(&atmosphere);
            ctx.arc(canvas.get_width() / 2, canvas.get_height() / 2, radius.ceil() as i32 + 8, 0.0, 2.0 * PI);
            ctx.fill();
        }

        canvas
    }
}