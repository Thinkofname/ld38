#![feature(conservative_impl_trait, link_args)]

extern crate libc;
extern crate rand;

pub mod js;
pub mod planet;
pub mod player;
pub mod object;
pub mod util;

use std::time;
use rand::{thread_rng, Rng};
use js::{JSElement, Canvas};
use std::rc::Rc;
use std::cell::Cell;
use std::f64::consts::PI;

pub const INITIAL_SIZE: f64 = 25.0;

// TODO:
// * Game over screen

fn main() {

    let body = js::get_body();
    let (mut width, mut height) = (body.get_client_width(), body.get_client_height());

    js::hook_events("main");
    let canvas = Canvas::get_by_id("main").unwrap();
    let ctx = canvas.get_context_2d();
    ctx.set_image_smoothing(false);

    canvas.set_width(width);
    canvas.set_height(height);

    ctx.set_fill_style("#00000");
    ctx.fill_rect(0, 0, width, height);

    // Help messages
    let mut help_messages = vec![];
    let help_movement = HelpText::new("Press  A  or  D  to move,  W  to jump");
    help_messages.push(help_movement.clone());
    let help_placement = HelpText::new("Press  E  to place a gun");
    help_messages.push(help_placement.clone());
    let help_fly = HelpText::new("Press  F  at the yellow seat to fly");
    help_messages.push(help_fly.clone());
    let help_fly_move = HelpText::new("Use  WASD  to fly");
    help_messages.push(help_fly_move.clone());
    let help_shoot = HelpText::new("Left click to shoot, use the mouse to aim");
    help_messages.push(help_shoot.clone());


    let mut player = player::Player::new(INITIAL_SIZE + INITIAL_SIZE);
    let mut planets = vec![];
    planets.push({
        let mut p = planet::Planet::new(0.0, 0.0, INITIAL_SIZE);
        p.objects.push(object::Object::new(
            object::ObjectType::Controller,
            INITIAL_SIZE + 1.0, 0.0
        ));
        p.max_health = 300.0;
        p.health = 260.0;
        p.player_owned = true;
        p
    });

    let mut rng = thread_rng();
    let mut stars: Vec<(f64, f64, f64)> = vec![];
    let mut projectiles: Vec<object::Projectile> = vec![];
    let mut fragments: Vec<Fragment> = vec![];

    // Force a regen
    width = -1;
    let mut last_frame = time::Instant::now();

    let mut next_wave_timer = 5.0;
    let mut in_wave = false;
    let mut wave = 0;

    let mut matter = 0;

    js::main_loop(move || {
        let start = time::Instant::now();
        let diff = start.duration_since(last_frame);
        last_frame = start;
        let delta =
            (diff.as_secs() * 1_000_000_000 + diff.subsec_nanos() as u64) as f64 / (1_000_000_000.0 / 60.0);
        let delta = delta.min(3.0);

        help_movement.mark_state(player.has_moved);
        help_placement.mark_state(player.has_placed);
        help_fly.mark_state(player.driving);
        help_fly_move.mark_state(player.has_fly_moved);
        help_shoot.mark_state(js::get_mouse_state().buttons & 1 == 1);

        if !in_wave && (wave != 0 || player.driving) && next_wave_timer <= 0.0 {
            in_wave = true;
            wave += 1;
            println!("Wave {}", wave);

            let (x, y) = if let Some(first) = planets.first() {
                (first.x, first.y)
            } else { (0.0, 0.0) };
            spawn_wave(x, y, wave, &mut planets);
        }
        if in_wave && planets.len() == 1 {
            in_wave = false;
            next_wave_timer = 15.0;
            player.driving = false;
        }

        let (cur_width, cur_height) = (body.get_client_width(), body.get_client_height());
        if width != cur_width || height != cur_height {
            width = cur_width;
            height = cur_height;
            canvas.set_width(width);
            canvas.set_height(height);
            stars.clear();
            for _ in 0 .. (width * height) / 10000 {
                stars.push((
                    rng.gen_range(0.1, 0.7),
                    rng.gen_range(0.0, width as f64),
                    rng.gen_range(0.0, height as f64),
                ))
            }
        }

        if let Some((first, ais)) = planets.split_first_mut() {
            for ai in ais {
                ai.ai_tick(first, delta);
            }
        }

        if let Some(first) = planets.first_mut() {
            player.tick(first, delta);
        }
        let mut increase = 0.0;
        let mut guns = 0;
        for p in &mut planets {
            p.tick(&mut projectiles, delta);
            if p.health <= 0.0 && !p.player_owned {
                increase += p.radius;
                guns += p.objects.len();
                matter += p.radius as i32;
            }
            if p.health <= 0.0 {
                for _ in 0 .. (p.radius / 4.0) as i32 {
                    fragments.push(Fragment {
                        color: p.ptype.water_color.clone(),
                        lifetime: 120.0,
                        size: rng.gen_range(4.0, p.radius / 6.0),
                        x: p.x + rng.gen_range(-p.radius, p.radius),
                        y: p.y + rng.gen_range(-p.radius, p.radius),
                        vx: rng.gen_range(-13.0, 13.0),
                        vy: rng.gen_range(-13.0, 13.0),
                    });
                }
            }
        }
        if let Some(player) = planets.first_mut() {
            player.increase_size(increase);
        }
        player.gun_parts += guns as f64 * 0.2;
        planets.retain(|v| v.player_owned || v.health > 0.0);

        // Resolve planet collisions
        let len = planets.len();
        for i in 1 .. len {
            let (before, after) = planets.split_at_mut(i);
            let current = before.last_mut().unwrap();
            for other in after {
                let dx = current.x - other.x;
                let dy = current.y - other.y;
                let com = current.radius + other.radius;
                if dx*dx + dy*dy <= com*com {
                    let (cf, of) = if current.radius > other.radius {
                        (0.8, 1.5)
                    } else {
                        (1.5, 0.8)
                    };
                    current.x -= current.avx * delta;
                    current.y -= current.avy * delta;
                    current.avx = -current.avx * cf;
                    current.avy = -current.avy * cf;
                    other.x -= other.avx * delta;
                    other.y -= other.avy * delta;
                    other.avx = -other.avx * of;
                    other.avy = -other.avy * of;
                }
            }
        }

        for p in &mut projectiles {
            p.tick(delta, &mut planets);
        }
        projectiles.retain(|p| !p.should_remove());


        ctx.set_fill_style("#000000");
        ctx.fill_rect(0, 0, width, height);

        for star in &stars {
            let (x, y) = if let Some(first) = planets.first() {
                (
                    star.1 - first.x * star.0,
                    star.2 - first.y * star.0,
                )
            } else { (star.1, star.2) };
            let x = (width as f64 + x % (width as f64)) % (width as f64);
            let y = (height as f64 + y % (height as f64)) % (height as f64);
            ctx.set_fill_style(&format!("rgba(255, 255, 0, {})", star.0));
            ctx.fill_rect(x as i32 - 1, y as i32 - 1, 2, 2);
        }

        ctx.save();

        let (sx, sy, px, py) = if let Some(first) = planets.first() {
            let sx = first.x as i32 - (width/2);
            let sy = first.y as i32 - (height/2);
            ctx.translate(
                -sx,
                -sy,
            );
            (sx, sy, first.x, first.y)
        } else { (0, 0, 0.0, 0.0) };

        for frag in &mut fragments {
            frag.x += frag.vx * delta;
            frag.y += frag.vy * delta;
            frag.lifetime -= delta;
            ctx.set_global_alpha((frag.lifetime / 120.0).max(0.0));
            ctx.begin_path();
            ctx.set_fill_style(&frag.color);
            ctx.arc(frag.x as i32, frag.y as i32, frag.size as i32, 0.0, 2.0 * PI);
            ctx.fill();
        }
        fragments.retain(|v| v.lifetime > 0.0);
        ctx.set_global_alpha(1.0);

        let fw = width as f64;
        let fh = height as f64;
        for p in &mut planets {
            p.draw(&ctx);
            let tx = p.x - sx as f64;
            let ty = p.y - sy as f64;
            if tx < 0.0 || ty < 0.0 || tx > fw || ty > fh {
                ctx.set_font("24px Monofett");
                ctx.set_fill_style("rgb(255, 0, 0)");
                let mut dx = p.x - px;
                let mut dy = p.y - py;
                let len = dx.hypot(dy);
                dx = (dx / len) * (fw.min(fh) * 0.5 - 32.0);
                dy = (dy / len) * (fw.min(fh) * 0.5 - 32.0);
                ctx.fill_text("X", (px + dx) as i32, (py + dy) as i32);
            }
        }
        for p in &mut projectiles {
            p.draw(&ctx);
        }
        if let Some(first) = planets.first() {
            player.draw(first, &ctx);
        }
        ctx.restore();

        if (wave != 0 || player.driving) && !in_wave {
            ctx.set_font("38px Monofett");
            ctx.set_fill_style("rgb(255, 255, 0)");
            ctx.fill_text(&format!("Next wave in: {:.0} seconds", next_wave_timer), 16, 100);
            next_wave_timer -= delta / 60.0;
        } else {
            next_wave_timer = 5.0;
        }
        ctx.set_font("38px Monofett");
        ctx.set_fill_style("rgb(0, 255, 0)");
        ctx.fill_text(&format!("Gun parts: {}", player.gun_parts.floor()), 16, 50);

        let mut offset = 0;
        for help in &help_messages {
            if help.hidden.get() && help.fade_timer.get() <= 0.0 {
                continue;
            }

            help.displayed.set(true);
            ctx.set_fill_style(&format!("rgba(0, 255, 255, {})", help.fade_timer.get() / 80.0));
            ctx.fill_text(help.text, 16, 150 + 50 * offset);
            offset += 1;

            if !help.hidden.get() {
                break;
            } else {
                help.fade_timer.set(help.fade_timer.get() - delta);
            }
        }

        if planets.first().map_or(false, |v| v.health <= 0.0) {
            ctx.set_fill_style("#FF0000");
            ctx.fill_text("Game Over", 16, 150 + 50);
            ctx.fill_text("Reload to play again", 16, 150 + 100);
            ctx.fill_text(&format!("You destroyed {} matter", matter), 16, 150 + 150);
        }
    });
}

fn spawn_wave(x: f64, y: f64, wave: i32, planets: &mut Vec<planet::Planet>) {
    use std::f64::consts::PI;
    let mut rng = thread_rng();
    match wave {
        1 => {
            planets.push(create_planet(x - 2000.0, y, 50.0, 10));
            planets.push(create_planet(x, y - 2000.0, 25.0, 5));
        }
        _ => {
            for _ in 0 .. wave + 1 {
                let ang = rng.gen_range(0.0, PI * 2.0);
                let size = rng.gen_range(30.0, 50.0 + wave as f64 * 25.0);
                let guns = rng.gen_range(1, (size / 5.0) as i32);
                let mut p = create_planet(x + 2000.0 * ang.cos(), y + 2000.0 * ang.sin(), size, guns);
                p.max_health = 100.0 + wave as f64 * 100.0;
                p.health = p.max_health;
                planets.push(p);
            }
        },
    }
}

fn create_planet(x: f64, y: f64, radius: f64, guns: i32) -> planet::Planet {
    let mut p = planet::Planet::new(x, y, radius);
    let mut rng = thread_rng();
    for _ in 0 .. guns {
        p.objects.push(object::Object::new(
            object::ObjectType::AIGun {
                initial_fire_delay: rng.gen_range(0.1, 30.0),
                cooldown: 0.0,
                max_cooldown: 120.0,
                shooting: false,
                target: None,
            },
            radius + 50.0,
            rng.gen_range(0.0, PI * 2.0),
        ))
    }
    p
}

struct HelpText {
    pub displayed: Cell<bool>,
    pub hidden: Cell<bool>,
    pub text: &'static str,
    pub fade_timer: Cell<f64>,
}

impl HelpText {
    pub fn new(msg: &'static str) -> Rc<HelpText> {
        Rc::new(HelpText {
            text: msg,
            displayed: Cell::new(false),
            hidden: Cell::new(false),
            fade_timer: Cell::new(80.0),
        })
    }

    pub fn mark_state(&self, cond: bool) {
        if self.displayed.get() && cond {
            self.hidden.set(true);
        }
    }
}

struct Fragment {
    lifetime: f64,
    x: f64,
    y: f64,
    vx: f64,
    vy: f64,
    color: String,
    size: f64,
}