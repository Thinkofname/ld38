
use js::{self, Canvas, Context2d};
use planet::Planet;
use std::char;
use std::f64::consts::PI;
use std::collections::HashMap;
use object::{Object, ObjectType};
use rand::{thread_rng, Rng};

const PLANET_SPEED: f64 = 7.0;

pub struct Player {
    image: Canvas,
    pub distance: f64,
    pub angle: f64,

    velocity: f64,
    scale: f64,

    pub driving: bool,

    key_state: HashMap<char, bool>,

    pub gun_parts: f64,

    // for help messages
    pub has_moved: bool,
    pub has_placed: bool,
    pub has_fly_moved: bool,
}

impl Player {
    pub fn new(distance: f64) -> Player {
        let canvas = Canvas::new();
        canvas.set_width(12);
        canvas.set_height(16);
        let ctx = canvas.get_context_2d();
        ctx.set_fill_style("#FFFFFF");
        ctx.fill_rect(0, 0, 12, 16);

        Player {
            image: canvas,
            distance: distance,
            angle: PI,
            velocity: 0.0,
            key_state: HashMap::default(),
            scale: 1.0,
            driving: false,
            gun_parts: 5.0,
            has_moved: false,
            has_placed: false,
            has_fly_moved: false,
        }
    }

    pub fn tick(&mut self, planet: &mut Planet, delta: f64) {
        let mut rng = thread_rng();
        while let Some(key) = js::poll_key_event() {
            match (char::from_u32(key.key as u32), key.up) {
                (Some('E'), true) if !self.driving => {
                    if self.gun_parts >= 1.0 {
                        if let None = planet.near_object(self.angle) {
                            self.gun_parts -= 1.0;
                            self.has_placed = true;
                            let mut obj = Object::new(
                                ObjectType::BasicGun {
                                    initial_fire_delay: rng.gen_range(0.1, 30.0),
                                    cooldown: 0.0,
                                    max_cooldown: 60.0,
                                    shooting: false,
                                },
                                self.distance + 1.0, self.angle
                            );
                            obj.velocity = 3.0;
                            planet.objects.push(obj);
                        }
                    }
                },
                (Some('F'), true) => {
                    if let Some(obj) = planet.near_object(self.angle).and_then(|v| planet.objects.get(v)) {
                        if let ObjectType::Controller = obj.otype {
                            self.driving = !self.driving;
                            if self.driving {
                                self.angle = obj.angle;
                            }
                        }
                    }
                },
                (Some(k), up) => *self.key_state.entry(k).or_insert(false) = !up,
                _ => {},
            }
        }
        if planet.health <= 0.0 {
            return;
        }

        if !self.driving {
            if *self.key_state.get(&'A').unwrap_or(&false) {
                self.has_moved = true;
                self.angle -= 0.1 * delta * (1.0 / (planet.radius / 25.0));
            } else if *self.key_state.get(&'D').unwrap_or(&false) {
                self.has_moved = true;
                self.angle += 0.1 * delta * (1.0 / (planet.radius / 25.0));
            }
            planet.vx = 0.0;
            planet.vy = 0.0;
        } else {
            if *self.key_state.get(&'A').unwrap_or(&false) {
                planet.vx = -PLANET_SPEED;
                self.has_fly_moved = true;
            } else if *self.key_state.get(&'D').unwrap_or(&false) {
                planet.vx = PLANET_SPEED;
                self.has_fly_moved = true;
            } else {
                planet.vx = 0.0;
            }
            if *self.key_state.get(&'W').unwrap_or(&false) {
                planet.vy = -PLANET_SPEED;
                self.has_fly_moved = true;
            } else if *self.key_state.get(&'S').unwrap_or(&false) {
                planet.vy = PLANET_SPEED;
                self.has_fly_moved = true;
            } else {
                planet.vy = 0.0;
            }
        }

        let angle = self.angle - (PI/2.0);
        let x = self.distance * angle.cos();
        let y = self.distance * angle.sin();
        let dx = x;
        let dy = y;
        if (dx*dx + dy*dy) - (planet.radius+8.0)*(planet.radius+8.0) > 0.5 {
            self.velocity -= 0.05 * delta;
        } else {
            if self.velocity < 0.0 {
                self.velocity = 0.0;
            } else {
                if !self.driving && *self.key_state.get(&'W').unwrap_or(&false) {
                    self.velocity = 2.0;
                }
            }
            self.distance = planet.radius + 8.0;
        }
        if self.velocity < -1.5 {
            self.velocity = -1.5;
        }

        self.distance += self.velocity * delta;

        let target_scale = if self.velocity.abs() < 0.01 {
            1.0
        } else {
            1.0 / self.velocity.abs()
        };
        self.scale += (target_scale - self.scale) * delta * 0.1;
    }

    pub fn draw(&mut self, planet: &Planet, ctx: &Context2d) {
        if planet.health <= 0.0 {
            return;
        }
        ctx.save();
        let angle = self.angle - (PI/2.0);
        ctx.translate(
            (planet.x + self.distance * angle.cos()) as i32,
            (planet.y + self.distance * angle.sin()) as i32,
        );
        ctx.rotate(self.angle);
        let scale = self.scale.min(1.0).max(0.1);
        ctx.scale(scale, 1.0 / scale);
        ctx.draw_image(
            &self.image,
            -6, -8
        );

        if let Some(obj) = planet.near_object(self.angle).and_then(|v| planet.objects.get(v)) {
            if !self.driving {
                if let ObjectType::Controller = obj.otype {
                    ctx.set_font("38px Monofett");
                    ctx.set_fill_style("rgb(255, 255, 0)");

                    ctx.save();
                    ctx.rotate(PI);
                    ctx.fill_text("^", -6, 35);
                    ctx.restore();

                    ctx.set_font("25px Monofett");

                    ctx.fill_text("F", -8, -30);
                }
            }
        }
        ctx.restore();
    }
}