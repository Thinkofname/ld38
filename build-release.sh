export EMCC_CFLAGS="-O3"
cargo build --release --target=asmjs-unknown-emscripten
rm -rf out/*
mkdir out
cp index.html out/index.html
cp -rf assets out/assets
cp target/asmjs-unknown-emscripten/release/*.* out/
cp target/asmjs-unknown-emscripten/release/deps/ld38-*.* out/
rm out/ld38-*.js
